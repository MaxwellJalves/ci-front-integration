# O git lab ele trabalha baseado em estagios.

    - build
    - teste
    - deploy

# Como definimos o estagio que queremos executar ?

- Através da palavra reservada "stage"
  - passo um subir uma imagem docker com as mesmas configurações de produção.
- stage: build

- Exemplo

```
image: docker:stable

build-imgdocker:
  stage: build
  scriot:
  - docker build -t minha-imagem .
```

para deixar o serviço do docker habilitado utilizamos a palavra reservada "Services"

docker em docker

```
services:
- docker:dind
```

utilizando o script antes de tudo`

```
before_script:
-   docker info
```

# Estudo do CI/CD o docker file

- Objetivo:

  - revisar o conceitos de build e package do java
  - revisar o controle de versão do java
  - aprimorar conceitos do DockerFile

- Após gerar as instruções do docker será necessário gerar o build da image

```
 docker build -t development:1.0 .
```

- Após a geração da imagem precisamos executar efetivamente a imagem.

```
docker run -p 8082:8082 development:1.0
```
