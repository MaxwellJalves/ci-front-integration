import React from "react";

import Formulario from "./components/Formulario/Formulario";
import Lista from "./components/Lista/Lista";
import ListaAutoComplite from "./components/ListaAutoComplite/ListaAutoComplite";

const App: React.FC = () => {
  const tarefas = [
    {
      descricao: "estudar react",
      tempo: "00:20",
    },
    {
      descricao: "Trabalhar ",
      tempo: "00:40",
    },
    {
      descricao: "dormir ",
      tempo: "00:40",
    },
    {
      descricao: "acordar ",
      tempo: "00:40",
    },
  ];
  function montarUrl() {
    document.location.search.replace("/", "/?");
    document.location.search = `tarefas=${tarefas.reduce((acc, currente) => {
      const [key, value] = currente.descricao.split("=");
      console.log(key, "valor", value);
      return {
        ...acc,

        [key]: [{ key, value }],
      };
    })}`;
  }
  return (
    <>
      <Formulario />
      <Lista lista={tarefas} />
      <button type="button" onClick={montarUrl}>
        GerarUrl
      </button>
      <div>
        <ListaAutoComplite />
      </div>
    </>
  );
};

export default App;
