interface IAtividades {
  descricao: string;
  tempo: string;
}

interface ILista {
  lista: IAtividades[];
}

const Lista = (props: ILista) => {
  return (
    <aside>
      <h2>Atividades</h2>
      <ul>
        {props.lista.map(({ descricao, tempo }) => (
          <li key={props.lista.length}>
            <h2>{descricao}</h2>
            <span>{tempo}</span>
          </li>
        ))}
      </ul>
    </aside>
  );
};
export default Lista;
