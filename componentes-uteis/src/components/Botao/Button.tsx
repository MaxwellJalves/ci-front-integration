import React from "react";
type Notificacoes =
  | "INICIANDO EXPEDIENTE"
  | "PAUSA"
  | "PAUSA ALMOÇO"
  | "FINALIZANDO EXPEDIENTE";

interface IButon {
  texto: string;
  descricao?: string;
  notificacao?: Notificacoes;
}

const Button = (props: IButon) => {
  const data = new Date();
  return (
    <button
      onClick={() => {
        if (props.notificacao !== undefined) {
          alert(
            `${props.notificacao} \n Dia ${data.getDate()} / ${
              data.getMonth() + 1
            } / ${data.getFullYear()}`
          );
        }
      }}
    >
      {props.texto}
    </button>
  );
};

export default Button;
