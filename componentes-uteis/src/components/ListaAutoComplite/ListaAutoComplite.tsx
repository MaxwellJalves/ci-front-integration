const Atividades = [
  "INICIAR JORNADA DE TRABALHO",
  "ESTUDO REACT",
  "ESTUDO NEXT",
  "MOMENTO DE LAZER",
];

interface Propriedade {
  placeholder?: string;
}

const ListaAutoComplite = (propriedades: Propriedade) => {
  return (
    <>
      <label htmlFor="atividades">Atividades</label>
      <input
        list="listar-atividades"
        id="atividades"
        placeholder="Atividades"
        onChange={(e) => console.log(e.target.value)}
      />

      <datalist id="listar-atividades">
        {Atividades.map((atividade) => (
          <option value={atividade} />
        ))}
      </datalist>
    </>
  );
};

export default ListaAutoComplite;
