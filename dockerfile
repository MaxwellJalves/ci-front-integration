FROM openjdk:11

RUN mkdir /api

COPY ./target/api-0.0.1.jar /api

EXPOSE 8082

ENTRYPOINT ["java","-jar" , "/api/api-0.0.1.jar"]