import Button from "../Botao/Button";

const Formulario = () => {
  return (
    <form>
      <div>
        <label htmlFor="tarefa">Tarefa</label>
        <input
          type="text"
          name="tarefa"
          id="tarefa"
          placeholder=" Informe sua a atividade que irá realizar"
          required
        />
      </div>
      <div>
        <label htmlFor="tempo">Horario</label>
        <input
          type="time"
          name="tempo"
          id="tempo"
          step="1"
          min="00:00:00"
          max="01:30:00"
          placeholder="00:00:00"
          required
        />
      </div>
      <Button texto="Registrar Usuario" />
    </form>
  );
};

export default Formulario;
